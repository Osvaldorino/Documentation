---
eleventyNavigation:
  key: AccessToken
  title: Generating an Access Token
  parent: AdvancedUsage
---

Access Tokens are special tokens that are used to authenticate to Codeberg through the [API](https://codeberg.org/api/swagger). They can be used to give applications access to your Codeberg account without sharing your password. 

> This guide explains how to generate an access token from the website. However, you can also generate access tokens from the API. Read more at [Gitea's documentation](https://docs.gitea.io/en-us/api-usage/#generating-and-listing-api-tokens).

## Generating an Access Token
Access tokens grant full access to your account. You must handle them securely.

<picture>
  <source srcset="/assets/images/advanced/access-token/generate-token.webp" type="image/webp">
  <img src="/assets/images/advanced/access-token/generate-token.png">
</picture>

1. In a web browser, go to Codeberg and click on the `Profiles and Settings` button on the top right. The button will be your profile picture.
2. Click on [`Settings`](https://codeberg.org/user/settings) in the dropdown.
3. Navigate to the [`Applications`](https://codeberg.org/user/settings/applications) settings tab.
4. Under `Manage Access Tokens | Generate New Token`, enter a name to identify your token in the `Token Name` field. 
5. Click the generate token button.

<picture>
  <source srcset="/assets/images/advanced/access-token/token-generated.webp" type="image/webp">
  <img src="/assets/images/advanced/access-token/token-generated.png">
</picture>

Your access token has now been generated! Copy your token and store it securely.

> Be sure to generate new access tokens for each app you use, and delete them once they are not in use.

## Deleting an Access Token
Once you are done using an access token, you should delete it. Deleting an access token revokes access to your account for apps using the token.

To delete an access token, click the red `Delete` button next to the access token you want to delete, then click `Yes` in the popup. Your access token is now deleted.